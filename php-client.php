<?php
//要在php-cli模式下运行
//php作为websocket客户端，触发websocket服务端主动推送消息给js客户端。
require "./WebSocketClient.php";
$host = '127.0.0.1';
$prot = 9502;
$client = new WebSocketClient($host, $prot);
$data = $client->connect();
$client->send("hello swoole, number:");//可以传送特定js客户对于的标识id，来触发服务端传送数据给特定用户
$client->close();