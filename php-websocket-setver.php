<?php
//要在php-cli模式下运行
//需要安装swoole拓展
//https://wiki.swoole.com/wiki/page/397.html
$ws = new swoole_websocket_server("0.0.0.0",9502);

$ws->on('open',function($ws,$request){
    //$request->fd客户端的标识id
    //$request->get客户端通过get方式传送过来的信息
    //可以把标识和get传送的信息关联起来存在redis或者mencache或者数据库供后台主动推送数据给特定的客户端
    var_dump($request->fd,$request->get,$request->server);
    $ws->push($request->fd,"hello,welcome\n");
});

$ws->on('message',function($ws,$frame){
    echo "Message:{$frame->data}\n";
    $ws->push($frame->data,"server:{$frame->data}");
});

$ws->on('close', function ($ws, $fd) {
    echo "client-{$fd} is closed\n";
});

$ws->start();